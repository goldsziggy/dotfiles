#!/usr/bin/python

from appscript import *
import requests
import random
import shutil
import os

image_directory= "/Users/zyg9085/DesktopImages/"
count = 20
subreddit = 'earthporn'
api_url = "https://www.reddit.com/r/" + subreddit + "/top/.json?count=" + str(count) + "&t=day"
reddit_username = "python-brainlessziggy"

def __main__():
  response = requests.get(api_url, headers={"User-Agent": reddit_username})
  response.raise_for_status()
  # access JSOn content
  data = response.json()
  #rand = random.randint(0, len(data['data']['children']-1))
  post = random.choice(data['data']['children'])['data']
  #post = data['data']['children'][rand]['data']
  image_url = post['url']
  filename = image_url.split("/")[-1]
  r = requests.get(image_url, stream = True)
  # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
  r.raw.decode_content = True# Open a local file with wb ( write binary ) permission.

  f = image_directory + filename
  with open(f,'wb') as file:
      shutil.copyfileobj(r.raw, file)
  
  os.system("/usr/local/bin/desktoppr " + f)
__main__()
