# My dotfiles

steps after copying....

```
:call dein#install()

:CocInstall coc-eslint coc-prettier coc-css coc-json coc-tsserver coc-pairs coc-markdownlint coc-styled-components coc-html coc-tabnine

`npm install -g flow-bin`
```

## NVIM Caveats

I altered the behavior of nerdtree, after `PlugInstall` you will have to symlink the included `plugged` folder to retain behavior of tabs
