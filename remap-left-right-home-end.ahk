﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;;; Clipboard
!c::
    send ^c
return

!v::
    send ^v
return


;;; Basic Movement

!left::
    send {home}
return

!right::
    send {end}
return

!up::
    send ^{home}
return

!down::
    send ^{end}
return

^left::
    send #^{left}
return

^right::
    send #^{right}
return

;;; Basic Movement with highlight

+!left::
    send +{home}
return

+!right::
    send +{end}
return

+!up::
    send +^{home}
return

+!down::
    send +^{end}
return



