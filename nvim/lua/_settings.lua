local set_options = function(locality,options)
local scopes = {o = vim.o, b = vim.bo, g = vim.g, w = vim.wo}
local scope = scopes[locality]
    for key, value in pairs(options) do
        scope[key] = value
    end
end

set_options("g", {
    mapleader = ",",
    javascript_plugin_jsdoc = 1,
    go_disable_autoinstall = 1,
    vim_json_syntax_conceal=0,
    jsx_ext_required=0,
    localvirmc_ask=0,
    webdevicons_enable_vimfiler=1,
    dracula_colorterm=0,
    airline_theme='dracula_pro',
    gitgutter_sign_added='✚',
    gitgutter_sign_modified = '✹',
    gitgutter_sign_removed = '-',
    gitgutter_sign_removed_first_line = '-',
    gitgutter_sign_modified_removed = '-',
    gitgutter_override_sign_column_highlight = 0,
    blamer_enabled = 1,
    blamer_show_in_visual_modes = 0,
    blamer_show_in_insert_modes = 0,
    doge_mapping = '<Leader>c',
    -- vimwiki_list = [{'path': '~/sandbox/notes/', 'path_html': '~/sandbox/notes/export'}]
})


set_options("o", {
    hlsearch = true, -- highlight matching search
    cmdheight=1,
    syntax= 'enable',
    title = true,
    shell='fish',
    cursorline = true, -- enable cursorline
    number = true, -- enable line numbers
    expandtab = true, -- this feature means that tabs are actually whitespaces so when we send code to friend indentation is not messed up
    shiftwidth = 2, -- when we hit tab it moves 4 spaces
    tabstop = 2, -- setting auto indent to 4 spaces
    softtabstop = 2, --option so make backspace delete entire tab
    ignorecase = true, -- ignore cases when searching
    smartcase = true, -- However if we use a capital in search string we then consider case-sensitivity, ignorecase is disabled
    smartindent = true, -- smarter indentation
    -- smarttab = true, -- make tab behaviour smarter
    undofile = true, -- Keeps undo history even after we close a file
    showmatch = true, -- match opening and closing braces
    lazyredraw = true, -- hopefully this speeds up vim!
    -- autoread = true, -- loads file as soon as there are changes on disk
    -- wildmenu = true, -- enhanced tab completion for vim command bar
    wildmode = "list,full", -- Displays a handy list of commands we can tab thru"
    hidden = true, -- ENABLE BUFFERS TO HIDE - PREVENTS ERROR: E37: No write since last change (add ! to override) When opening a new buffer before saving current one
    shiftround = true, -- Rounds the indent spacing to the next multiple of shiftwidth EG. If you have something 3 spaces in and hit < it will move 2 or 4 spaces depending on shiftwidth and line up
    shortmess = "aF", -- abreviates messages and prevents file name being echoed when opened
    backspace = "indent,eol,start", -- this makes backspace work as normal
    scrolloff = 5, -- Set the cursor 5 lines down instead of at the top
    sidescroll = 2, -- make scrolling better, instead of wrap we sroll horizontally with the cursor
    wrap = false, -- dont wrap lines
    encoding = "UTF-8", -- REQUIRED BY DEV ICONS PLUGIN
    history = 200, -- keep 200 hungy commands in the stash
    updatetime = 200, -- Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable " delays and poor user experience. FOR ASYNC STUFF ONLY
    timeoutlen = 400, -- faster timeout wait time
    spelllang = "en", -- Set NEOVIMS NATIVE spell lang for spellchecker
    inccommand = "nosplit", -- This is Neovim only. inccommand shows you in realtime what changes your ex command should make. Right now it only supports s,but even that is incredibly useful. If you type :s/regex, it will highlight what matches regex. If you then add /change, it will show all matches replaced with change. This works with all of the regex properties, include backreferences and groups.
    clipboard = "unnamedplus", -- share system clipboard but also retain nvim clipboard (see += compared
    mouse = "a", -- allows me to scroll with my touchpad in two different splits just by hoevering the mouse in the split I wish to scroll
})



