
-- Only required if you have packer configured as `opt`
vim.cmd [[packadd packer.nvim]]
-- Only if your version of Neovim doesn't have https://github.com/neovim/neovim/pull/12632 merged

return require('packer').startup(function()
-- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use 'tpope/vim-fugitive'
  use 'itchyny/lightline.vim'

  use {'neoclide/coc.nvim', branch='release', run=':source ~/.config/nvim/plugins/coc.rc.vim'}
  use {'lotabout/skim', rtp='~/.skim/', run='./install'}
  use {'kkoomen/vim-doge', run=':call doge#install()'}
  -- use {'Yggdroot/LeaderF', run=':LeaderfInstallCExtension'}

  use 'APZelos/blamer.nvim'
  use 'tpope/vim-commentary'
  use 'kyazdani42/nvim-web-devicons'
  use 'romgrk/barbar.nvim'
  use 'vim-airline/vim-airline'
  use 'leafgarland/typescript-vim'
  use 'peitalin/vim-jsx-typescript'
  use 'chemzqm/vim-jsx-improve'
  use 'yuezk/vim-js'
  use 'iamcco/markdown-preview.nvim'
  use 'heavenshell/vim-jsdoc'
  use 'airblade/vim-gitgutter'
  use 'sheerun/vim-polyglot'
  use 'lotabout/skim.vim'
  use 'suy/vim-context-commentstring'

  use 'vimwiki/vimwiki'
  use 'mattn/calendar-vim'
  
  use {'OmniSharp/omnisharp-vim', ft='cs'}
  use {'HerringtonDarkholme/yats.vim', ft='typescriptreact'}
  use {'elzr/vim-json', ft='json'}
  use {'stephpy/vim-yaml', ft='yaml'}
  use {'dag/vim-fish', ft='fish'}
  use {'cespare/vim-toml', ft='toml'}
  use {'github/copilot.vim'}
  use {
      "nvim-telescope/telescope.nvim",
      requires = {
         {
           "nvim-lua/plenary.nvim"
         },
         {
            "nvim-telescope/telescope-fzf-native.nvim",
            run = "make",
         },
         {
            "nvim-telescope/telescope-media-files.nvim"
         }
      },
   }
   -- smooth scroll
   use {
      "karb94/neoscroll.nvim",
      config = function()
         pcall(function()
              require("neoscroll").setup()
          end)
      end,
   }
   -- use { 
   --  "nvim-neorg/neorg",
   --  config = function()
   --      require('neorg').setup {
   --          -- Tell Neorg what modules to load
   --          load = {
   --              ["core.defaults"] = {}, -- Load all the default modules
   --              ["core.norg.concealer"] = {}, -- Allows for use of icons
   --              ["core.norg.dirman"] = { -- Manage your directories with Neorg
   --                  config = {
   --                      workspaces = {
   --                          my_workspace = "~/neorg"
   --                      }
   --                  }
   --              }
   --          },
   --      }
   --  end,
   --  requires = "nvim-lua/plenary.nvim"
  -- }

  -- use {
  -- 'nvim-telescope/telescope.nvim',
  -- requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}, {'nvim-telescope/telescope-fzy-native.nvim'}},
  -- module_patterns = "telescope.*"
  -- }
  -- Post-install/update hook with neovim command
  -- use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
end)



