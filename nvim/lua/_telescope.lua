local present, telescope = pcall(require, "telescope")
if not present then
   return
end

telescope.setup {
   defaults = {
      vimgrep_arguments = {
         "rg",
         "--color=never",
         "--no-heading",
         "--with-filename",
         "--line-number",
         "--column",
         "--smart-case",
      },
      prompt_prefix = "   ",
      selection_caret = "  ",
      entry_prefix = "  ",
      initial_mode = "insert",
      selection_strategy = "reset",
      sorting_strategy = "descending",
      layout_strategy = "horizontal",
      layout_config = {
         horizontal = {
            prompt_position = "top",
            preview_width = 0.55,
            results_width = 0.8,
         },
         vertical = {
            mirror = false,
         },
         width = 0.87,
         height = 0.80,
         preview_cutoff = 120,
      },
      file_sorter = require("telescope.sorters").get_fuzzy_file,
      file_ignore_patterns = {},
      generic_sorter = require("telescope.sorters").get_generic_fuzzy_sorter,
      path_display = { "absolute" },
      winblend = 0,
      border = {},
      borderchars = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
      color_devicons = true,
      use_less = true,
      set_env = { ["COLORTERM"] = "truecolor" }, -- default = nil,
      file_previewer = require("telescope.previewers").vim_buffer_cat.new,
      grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
      qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new,
      -- Developer configurations: Not meant for general override
      buffer_previewer_maker = require("telescope.previewers").buffer_previewer_maker,
   },
   extensions = {
      fzf = {
         fuzzy = true, -- false will only do exact matching
         override_generic_sorter = false, -- override the generic sorter
         override_file_sorter = true, -- override the file sorter
         case_mode = "smart_case", -- or "ignore_case" or "respect_case"
         -- the default case_mode is "smart_case"
      },
      media_files = {
         filetypes = { "png", "webp", "jpg", "jpeg" },
         find_cmd = "rg", -- find command (defaults to `fd`)
      },
   },
}

-- NvChad pickers
-- load the theme_switcher extension
-- require("telescope").load_extension "themes"
-- load the term_picker extension
-- require("telescope").load_extension "terms"

if not pcall(function()
   telescope.load_extension "fzf"
   telescope.load_extension "media_files"
end) then
   -- This should only trigger when in need of PackerSync, so better do it
   print "After completion of PackerCompile, restart neovim."
   -- Trigger packer compile on PackerComplete, so it properly waits for PackerSync
   vim.cmd 'autocmd User PackerComplete ++once lua print "Waiting for PackerCompile.." require("packer").compile()'
   vim.cmd 'autocmd User PackerCompileDone ++once echo "Packer Compile done, restart neovim."'
   -- require "pluginList"
   require("packer").update("telescope-fzf-native.nvim", "telescope-media-files.nvim")
end





-- local actions = require('telescope.actions')
-- local telescope = require('telescope')
-- telescope.setup{
--     config = {
--         mappings = {i = {["<esc>"] = actions.close,
--         ["<C-c>"] = function()
--             vim.cmd [[stopinsert]] --start normal mode if we need it
--         end,
--     }},
--     vimgrep_arguments = {
--         'rg',
--         '--color=never',
--         '--no-heading',
--         '--with-filename',
--         '--line-number',
--         '--column',
--         '--smart-case',
--     },
--     -- find_command = { 'rg', '--files', '--hidden'},
--     -- prompt_position = "top",
--     -- prompt_prefix = ">",
--     initial_mode = "insert",
--     -- scroll_strategy ="limit", CAUSING BUG [Jun2021]
--     selection_strategy = "reset",
--     sorting_strategy = "ascending",
--     layout_strategy = "horizontal",
--     layout_defaults = {
--         -- TODO add builtin options.
--     },
--     file_sorter =  require'telescope.sorters'.get_fuzzy_file,
--     file_ignore_patterns = {"forks",".backup",".swap",".langservers",".session",".undo",".git/","node_modules","vendor",".cache",".vscode-server",".Desktop",".Documents","classes","*-lock.json"},
--     generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
--     -- shorten_path = true,
--     winblend = 0,
--     -- width = 0.75,
--     -- preview_cutoff = 120,
--     -- results_height = 1,
--     -- results_width = 0.8,
--     border = {},
--     borderchars = { '─', '│', '─', '│', '╭', '╮', '╯', '╰'},
--     color_devicons = true,
--     use_less = true,
--     -- set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
--     -- file_previewer = require'telescope.previewers'.cat.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_cat.new`
--     -- grep_previewer = require'telescope.previewers'.vimgrep.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_vimgrep.new`
--     -- qflist_previewer = require'telescope.previewers'.qflist.new, -- For buffer previewer use `require'telescope.previewers'.vim_buffer_qflist.new`

--     -- Developer configurations: Not meant for general override
--     -- buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker
-- },
-- extensions = {
--     fzy_native = {
--         override_generic_sorter = true,
--         override_file_sorter = true,
--     },
-- },

-- }

-- -- vim.cmd[[highlight TelescopeBorder guifg=#4c4c4c]]
-- -- vim.cmd[[highlight TelescopeSelection guifg=#ffffff guibg=#393939 gui=bold]]
-- -- vim.cmd[[highlight TelescopeSelectionCaret guifg=#749484 gui=bold]]

-- -- require('telescope').load_extension('fzy_native')
-- require('telescope').load_extension('fzy_native')
