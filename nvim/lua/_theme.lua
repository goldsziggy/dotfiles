local cmd = vim.cmd

cmd('packadd! dracula_pro')

cmd('set background=dark')
cmd('highlight Visual cterm=NONE ctermbg=236 ctermfg=NONE guibg=Grey40')
cmd('highlight LineNr cterm=none ctermfg=240 guifg=#2b506e guibg=#000000')
cmd("set guifont=DankMono\\ Nerd\\ Font")
cmd('colorscheme dracula_pro')

