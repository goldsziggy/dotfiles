local execute = vim.api.nvim_command
local fn = vim.fn


-- === Install Packer from Github ===  
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'

if fn.empty(fn.glob(install_path)) > 0 then
  fn.system({'git', 'clone', 'https://github.com/wbthomason/packer.nvim', install_path})
  execute 'packadd packer.nvim'
end


require('plugins')
require('_theme')
require('_settings')
require('_CocExplorerBarBar')
require('_telescope')
-- require('_dashboard')
-- require('_misc_plugins')
-- require('_treesitter')
