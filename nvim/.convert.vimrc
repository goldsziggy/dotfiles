
" ========================
" ===== AUTO COMMANDS ====
" ========================

" Autocommand
autocmd!

function! s:NoOpenInSidebar()
  if &buftype != '' || win_gettype() != '' || nvim_win_get_config(0).relative != ''
    let w:is_sidebar = 1
  elseif get(w:, 'is_sidebar')
    let l:curbuf = bufnr()
    close
    exec 'buffer '.l:curbuf
  endif
endfunction

autocmd BufEnter * call <sid>NoOpenInSidebar()


autocmd User CocExplorerOpenPre lua require'bufferline.state'.set_offset(30, 'FileTree')
autocmd User CocExplorerQuitPre lua require'bufferline.state'.set_offset(0) 


" Turn off paste mode when leaving insert
autocmd InsertLeave * set nopaste

" Add Yaml Stuffz
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml " foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" JavaScript
au BufNewFile,BufRead *.es6 setf javascript

au BufNewFile,BufRead *.js setf javascript.jsx
" TypeScript
au BufNewFile,BufRead *.tsx setf typescript
" Markdown
au BufNewFile,BufRead *.md set filetype=markdown
" Flow
au BufNewFile,BufRead *.flow set filetype=javascript

" " set filetypes as typescriptreact
" au BufNewFile,BufRead *.tsx setf filetype=typescriptreact

augroup BgHighlight
  autocmd WinEnter * set cul
  autocmd WinLeave * set nocul
augroup END

" if &term =~ "screen"
"   autocmd BufEnter * if bufname("") !~ "^?[A-Za-z0-9?]*://" | silent! exe '!echo -n "\ek[`hostname`:`basename $PWD`/`basename %`]\e\\"' | endif
"   autocmd VimLeave * silent!  exe '!echo -n "\ek[`hostname`:`basename $PWD`]\e\\"'
" endif

function! EnableStyledComponentsCSS() abort
  if exists('b:current_syntax')
    let s:current_syntax=b:current_syntax
    unlet b:current_syntax
  endif
  syntax include @CSS syntax/css.vim
  if exists('s:current_syntax')
    let b:current_syntax=s:current_syntax
  else
    unlet b:current_syntax
  endif
  syntax region textSnipcss matchgroup=StyledComponents start=/styled.*`/ end="`" contains=@CSS
endfunction
autocmd FileType javascript.jsx call EnableStyledComponentsCSS()


" ========================
" === END AUTO COMMANDS ==
" ========================


" ========================
" === NMAP and MAP =======
" ========================
nmap <space>e :CocCommand explorer<CR>
nmap <space>f :CocCommand explorer --preset floating<CR>

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>


noremap [skim] <Nop>
map <Leader>f [skim]
nmap [skim]f :<C-u>Files ./<CR>
nmap [skim]F :<C-u>Files 
nmap <silent> [skim]g :<C-u>GFiles<CR>
nmap <silent> [skim]G :<C-u>GFiles?<CR>
nmap <silent> [skim]b :<C-u>Buffers<CR>
nmap [skim]L :<C-u>Lines 
nmap [skim]l :<C-u>BLines 
nmap [skim]T :<C-u>Tags 
nmap [skim]t :<C-u>BTags 
nmap <silent> [skim]m :<C-u>Marks<CR>  " TODO: Marks does not work!
nmap <silent> [skim]h :<C-u>History<CR>  " TODO: History does not work!
nmap <silent> [skim]: :<C-u>History:<CR>
nmap <silent> [skim]/ :<C-u>History/<CR>

" ========================
" === END NMAP and MAP ===
" ========================

scriptencoding utf-8


" ========================
" ====== PLUGINS =========
" ========================
" if !exists('*fugitive#statusline')
"   set statusline=%F\ %m%r%h%w%y%{'['.(&fenc!=''?&fenc:&enc).':'.&ff.']'}[L%l/%L,C%03v]
"   set statusline+=%=
"   set statusline+=%{fugitive#statusline()}
" endif
 " let g:vimwiki_list = [{'path': '~/sandbox/notes/', 'path_html': '~/sandbox/notes/export'}]
" let g:vimwiki_list = [{'path': '~/vimwiki/', 'auto_export': 1}]

let g:vimwiki_list = [{'path': '$HOME/vimwiki',
		  \ 'template_path': '$HOME/vimwiki_html/templates',
		  \ 'template_default': 'dark_template',
		  \ 'template_ext': '.html',
      \ 'auto_export': 1}]

let g:vimwiki_valid_html_tags = 'b,i,s,u,sub,sup,kbd,br,hr, pre, script'

command! Diary VimwikiDiaryIndex
augroup vimwikigroup
    autocmd!
    " automatically update links on read diary
    autocmd BufRead,BufNewFile diary.wiki VimwikiDiaryGenerateLinks
augroup end
au BufNewFile ~/vimwiki/diary/*.wiki :silent 0r !~/.config/nvim/generate-vimwiki-diary-template '%'
autocmd FileType vimwiki autocmd BufWritePost <buffer> silent Vimwiki2HTML

source ~/.vimrc.maps
" source ~/.vimrc.lightline

