#!/bin/bash
ln -s $(pwd)/.vimrc ~/.vimrc 
ln -s $(pwd)/.vimrc.lightline ~/.vimrc.lightline 
ln -s $(pwd)/.vimrc.maps ~/.vimrc.maps 
ln -s $(pwd)/.vimrc.osx ~/.vimrc.osx
ln -s $(pwd)/.convert.vimrc ~/.convert.vimrc
ln -s $(pwd)/init.vim ~/.config/nvim/init.vim
ln -s $(pwd)/coc-settings.json ~/.config/nvim/coc-settings.json
ln -s $(pwd)/generate-vimwiki-diary-template ~/.config/nvim/generate-vimwiki-diary-template

chmod +x ~/.config/nvim/generate-vimwiki-diary-template
mkdir -p  ~/.vim/rc; ln -s $(pwd)/dein_lazy.toml ~/.vim/rc/dein_lazy.toml
mkdir -p  ~/.vim/rc; ln -s $(pwd)/dein.toml ~/.vim/rc/dein.toml
mkdir -p  ~/.config/nvim/plugins; ln -s $(pwd)/plugins/* ~/.config/nvim/plugins
mkdir -p  ~/.config/nvim/lua; ln -s $(pwd)/lua/* ~/.config/nvim/lua
mkdir -p  ~/.config/nvim/colors; ln -s $(pwd)/colors/* ~/.config/nvim/colors
mkdir -p ~/vimwiki_html/templates; ln -s $(pwd)/vimwiki_templates/* ~/vimwiki_html/templates
mkdir -p ~/.local/share/nvim/site/pack/themes/start; cp -r $(pwd)/themes/dracula_pro ~/.local/share/nvim/site/pack/themes/start/dracula_pro

