#!/usr/local/bin/fish

tmux split-window -h
tmux select-pane -t 1
tmux resize-pane -R 66
tmux send-keys 'nvim .' C-m

tmux select-pane -t 2
tmux send-keys 'npm run dev'

tmux split-window -v

# tmux select-pane -t 3 -T "Fish"
# tmux select-pane -t 2 -T 'npm run dev'
# tmux select-pane -t 1 -T 'Editor'
# tmux set pane-border-status top
tmux set pane-border-status off
