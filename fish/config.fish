# chips
if [ -e ~/.config/chips/build.fish ] ; . ~/.config/chips/build.fish ; end


#asdad
#thefuck
thefuck --alias | source

alias vi "nvim"
alias nodejs node

alias fix-node-firewall "/Users/ZYG9085/sandbox/experiments/fix-node-firewall.sh"

alias grepjs 'grep -r --exclude-dir=node_modules --exclude-dir=.git'
#alias grep 'rg'
alias greprg 'rg'
alias grepBetter 'rg'
alias t 'dstask'
alias task 'dstask'

alias cat-package-script "cat package.json | jq '.scripts'"
alias cat-package-dep "cat package.json | jq '.dependencies'"
alias cat-package-dev-dep "cat package.json | jq '.devDependencies'"
alias cdma "cd /Users/ZYG9085/sandbox/micro-apps"
alias cdms "cd /Users/ZYG9085/sandbox/micro-services"
alias dev "~/.config/dotfiles/tmux-dev-layout.sh"
# alias dev "stmux --mouse -a z -- [ -s 5/6 [ \"nvim .\" ] : [ fish .. \"npm run dev\" ] ]"

# export EDITOR="nvim"
set -gx EDITOR nvim
set -g s "/Users/ZYG9085/sandbox/"

# https://github.com/nvbn/thefuck/issues/1219
set -gx THEFUCK_PRIORITY "git_hook_bypass=1100"

# TMUX
# Set the tmux window title, depending on whether we are running something, or just prompting function fish_title if [ "fish" != $_ ] tmux rename-window "$_ $argv" else tmux_directory_title end end

# function tmux_directory_title if [ "$PWD" != "$LPWD" ] set LPWD "$PWD" set INPUT $PWD set SUBSTRING (eval echo $INPUT| awk '{ print substr( $0, length($0) - 19, length($0) ) }') tmux rename-window "..$SUBSTRING" end end 

# Custom Overrides
if [ -e ~/.config/fish/nm-config.fish ] ; . ~/.config/fish/nm-config.fish ; end

# https://github.com/carlocab/tmux-nvr
#
# i:wf [ -n "$TMUX" ];
#     # set cmd "tmux show-environment -s NVIM_LISTEN_ADDRESS"
#     # $cmd
#     tmux show-environment -s NVIM_LISTEN_ADDRESS
# else
#     set NVIM_LISTEN_ADDRESS /tmp/nvimsocket
# end

# Cargo
fish_add_path $HOME/.cargo/bin
# source $HOME/.cargo/env
set -g fish_user_paths "/usr/local/sbin" $fish_user_paths
