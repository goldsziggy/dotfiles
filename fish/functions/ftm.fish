function ftm --argument-names 'input'
  set -l change ''
  if test -n "$TMUX"
    set change "switch-client"
  else
    set change "attach-session"
  end
  if test -n "$input"
    echo "$change blah"
    echo "$input test"
    tmux $change -t "$input" 2>/dev/null || tmux new-session -d -s $input && tmux $change -t "$input"; return
  end
  set session (tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux $change -t "$session" || echo "No sessions found."
end
