function ftmk --argument-names 'input'
  if test -n "$input"
    tmux kill-session -t "$input"; return
  end
  set session (tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux kill-session -t "$session" || echo "No session found to delete."
end

